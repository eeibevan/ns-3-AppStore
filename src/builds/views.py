from django.shortcuts import render
from apps.models import Build, Release, App
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from appstore.settings.base import PAGINATION_BUILDS_PER_PAGE


def build_history(request, app, release):
    try:
        release_app = App.objects.get(name=app)
        release = Release.objects.get(app=release_app, version=release)
    except (Release.DoesNotExist, App.DoesNotExist):
        context = {
            'message': "Requested App or Release don't exist!",
            'go_back_to_url': "/",
            'go_back_to_title': "Home Page",
        }
        return render(request, 'message.html', context)
    build_list = Build.objects.filter(release=release).order_by('-date_time')
    paginator = Paginator(build_list, PAGINATION_BUILDS_PER_PAGE)
    page = request.GET.get('page')
    try:
        builds = paginator.page(page)
    except PageNotAnInteger:
        builds = paginator.page(1)
    except EmptyPage:
        builds = paginator.page(paginator.num_pages)
    context = {
        'page': page,
        'release': release,
        'builds': builds
    }
    return render(request, 'build_history.html', context)
