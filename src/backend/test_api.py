import json
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from authtools.models import User
from apps.models import App, NsRelease, Release, Build
from django.core.files.uploadedfile import SimpleUploadedFile


class AppSearchAPIViewTestCase(APITestCase):
	url = reverse("backend:search")

	def setUp(self):
		DApp = App.objects.create(name='ns-3-gym',
							title='Gym',
							app_type='F',
							abstract='This is a test App for Development',
							description='This is a test App for Development')

		ns=NsRelease.objects.create(name='ns-3.29',url='https://www.nsnam.org/')
		Release.objects.create(app=DApp,version='TestVersion',require=ns,date= '2018-12-27',notes= 'TestNote',filename = SimpleUploadedFile('filename.txt',''),url='https://www.nsnam.org/')

	def test_search(self):
		"""
		Test to verify the search API Functionality
		"""
		# exisiting application
		response = self.client.post(self.url, {'q':'gym', 'ns':'ns-3.29'})
		self.assertEqual(200, response.status_code)

		# when no params are passed, returns all the apps
		response = self.client.post(self.url)
		self.assertEqual(200, response.status_code)
		self.assertTrue(len(json.loads(response.content)) > 0)

		# random keyword, no such app found
		response = self.client.post(self.url, {'q':'abcdefghi', 'ns': 'ns-3.21'})
		self.assertTrue(len(json.loads(response.content)) == 0)

		# when keyword is empty, no app found
		response = self.client.post(self.url, {'ns': 'ns-3.21'})
		self.assertTrue(len(json.loads(response.content)) == 0)


class AppInstallAPIViewTestCase(APITestCase):
	url = reverse("backend:install")

	def setUp(self):
		DApp = App.objects.create(name='ns3-gym',
							title='Gym',
							app_type='F',
							abstract='This is a test App for Development',
							description='This is a test App for Development')

		ns=NsRelease.objects.create(name='3.29',url='https://www.nsnam.org/')
		Release.objects.create(app=DApp,version='TestVersion',require=ns,date= '2018-12-27',notes= 'TestNote',filename = SimpleUploadedFile('filename.txt',''),url='https://www.nsnam.org/')


	def test_install(self):
		"""
		Test to verify the install API Functionality
		"""

		# when the app exists
		response = self.client.post(self.url, {'module_name':'ns3-gym', 'ns':'ns-3.29'})
		self.assertEqual(200, response.status_code)

		# app exists, with multiple ns versions
		response = self.client.post(self.url, {'module_name':'ns3-gym', 'ns':'[ns-3.29, ns-3.27]'})
		self.assertEqual(200, response.status_code)

		# random keyword, no such app found
		response = self.client.post(self.url, {'module_name':'abcdeghi', 'ns':'[ns-3.29, ns-3.27]'})
		self.assertEqual(404, response.status_code)

		# random with multiple ns versions, no such app found
		response = self.client.post(self.url, {'module_name':'abcdeghi', 'version': '1.10', 'ns':'[ns-3.29, ns-3.27]'})
		self.assertEqual(404, response.status_code)
		
class BuildUpdateAPIViewTestCase(APITestCase):
	def setUp(self):
		self.user = User.objects.create_user(email='test@email.com', password='strong_password')
		self.user.is_superuser = True
		self.user.is_staff = True
		self.user.save()
		self.app = App.objects.create(name='ns3-gym',
							title='Gym',
							app_type='F',
							abstract='This is a test App for Development',
							description='This is a test App for Development')

		self.ns=NsRelease.objects.create(name='3.29',url='https://www.nsnam.org/')
		self.release = Release.objects.create(app=self.app,version='TestVersion',require=self.ns,date= '2018-12-27',notes= 'TestNote',filename = SimpleUploadedFile('filename.txt',''),url='https://www.nsnam.org/')
		self.build = Build.objects.create(status=Build.ABORTED, release=self.release, pipeline_name='Test pipeline')
		self.url = reverse('backend:build', kwargs={"pk": self.build.pk})
		self.token = Token.objects.create(user=self.user)
		self.api_authentication()
		
	def api_authentication(self):
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
		
	def test_build_update(self):
		"""
		Test to verify the Build model object update
		"""
		#status is in correct range and build_id is valid
		response = self.client.patch(self.url, {'status': Build.BUILDING, 'url': 'https://www.nsnam.org/', 'build_id': 1})
		response_data = json.loads(response.content)
		self.assertEqual(200, response.status_code)
		self.assertEqual(response_data.get('status'), Build.BUILDING)
		self.assertEqual(response_data.get('url'), 'https://www.nsnam.org/')
		self.assertEqual(response_data.get('build_id'), 1)
		
		#status is incorrect
		response = self.client.patch(self.url, {'status': 5, 'url': 'https://www.nsnam.org/', 'build_id': 1})
		self.assertEqual(400, response.status_code)
	
	def test_for_other_user(self):
		"""
		Test to verify the Build model object is not updated by other user
		"""
		other_user = User.objects.create_user(email='test1@email.com', password='strong_password')
		other_user.save()
		new_token = Token.objects.create(user=other_user)
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)
		
		#status is in correct range and build_id is valid
		response = self.client.patch(self.url, {'status': Build.BUILDING, 'url': 'https://www.nsnam.org/', 'build_id': 1})
		self.assertEqual(403, response.status_code)