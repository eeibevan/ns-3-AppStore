# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from apps.models import App, NsRelease, Release
from util.parse_bake import get_dependency


class ParseBakeTestCase(TestCase):
    def setUp(self):
        ns = NsRelease.objects.create(
            name='Test', url='https://www.nsnam.org/')
        app = App.objects.create(
            name='App',
            title='App Title',
            app_type='F',
            abstract='This is a test App',
            description='This is a test App')
        Release.objects.create(
            app=app,
            version='TestVersion',
            require=ns,
            date='2018-12-27',
            notes='TestNote',
            url='https://www.nsnam.org/')

    def test_get_dependency(self):
        app = App.objects.get(name='App')
        release = Release.objects.get(version='TestVersion')
        optional_dependency, compulsory_dependency = get_dependency(
            app, release)
        self.assertEqual(optional_dependency, None)
        self.assertEqual(compulsory_dependency, None)